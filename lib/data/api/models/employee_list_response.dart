import 'package:arqathon_flutter_quick_start/domain/models/employee.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'package:arqathon_flutter_quick_start/data/api/models/employee_list_response.g.dart';

@JsonSerializable()
class EmployeeListResponse extends Object with _$EmployeeListResponseSerializerMixin {
  @JsonKey(name: 'next_page')
  final int nextPage;

  @JsonKey(name: 'employees')
  final List<Employee> employees;

  EmployeeListResponse({
    this.nextPage,
    @required this.employees,
  });

  factory EmployeeListResponse.fromJson(Map<String, dynamic> json) =>
      _$EmployeeListResponseFromJson(json);
}
