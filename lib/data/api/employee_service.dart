import 'package:arqathon_flutter_quick_start/environment.dart';
class EmployeeService {
  static String employeeListApi = "${environment.apiBaseUrl}/employee";

  /**
   * Get Employee Details by Employee Id
   * Example: /employee/id/1
   */
  static String employeeDetailsApi = "${environment.apiBaseUrl}/employee/id/";
}
