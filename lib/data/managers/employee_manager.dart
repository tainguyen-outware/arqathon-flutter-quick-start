import 'dart:async';
import 'dart:convert';

import 'package:arqathon_flutter_quick_start/data/api/employee_service.dart';
import 'package:arqathon_flutter_quick_start/data/api/models/employee_list_response.dart';
import 'package:arqathon_flutter_quick_start/domain/models/employee.dart';
import 'package:arqathon_flutter_quick_start/domain/repositories/employee_repository.dart';
import 'package:http/http.dart' as http;

class EmployeeManager implements EmployeeRepository {
  @override
  Future<EmployeeListResponse> getEmployees() async {
    var response = await http.get(EmployeeService.employeeListApi);
    if (response.statusCode == 200) {
      Map employeeListResponseMap = json.decode(response.body);
      return EmployeeListResponse.fromJson(employeeListResponseMap);
    } else {
      print("Server Error");
      //TODO handle Server Error here
      throw Error;
    }
  }

  @override
  Future<Employee> getEmployeeDetails(String employeeId) async {
    var response = await http.get(EmployeeService.employeeDetailsApi + employeeId);
    if (response.statusCode == 200) {
      Map employeeResponse = json.decode(response.body);
      return Employee.fromJson(employeeResponse);
    } else {
      //TODO handle error
      return null;
    }
  }
}
