import 'dart:io';

import 'package:arqathon_flutter_quick_start/data/api/models/employee_list_response.dart';
import 'package:arqathon_flutter_quick_start/data/managers/employee_manager.dart';
import 'package:arqathon_flutter_quick_start/data/network/loading_status.dart';
import 'package:arqathon_flutter_quick_start/domain/models/employee.dart';
import 'package:arqathon_flutter_quick_start/presentation/common/base_state_view.dart';
import 'package:arqathon_flutter_quick_start/presentation/common/loading_view.dart';
import 'package:arqathon_flutter_quick_start/presentation/common/network_error_view.dart';
import 'package:arqathon_flutter_quick_start/presentation/common/progress_indicator.dart';
import 'package:arqathon_flutter_quick_start/presentation/employee/list/list_item_employee.dart';
import 'package:arqathon_flutter_quick_start/presentation/employee/list/search/search_bar.dart';
import 'package:arqathon_flutter_quick_start/resources/assets.dart';
import 'package:arqathon_flutter_quick_start/utils/network_utils.dart';
import 'package:flutter/material.dart';

class EmployeeList extends StatefulWidget {
  static const String routeName = '/master';

  @override
  _EmployeeListState createState() => _EmployeeListState();
}

class _EmployeeListState extends State<EmployeeList> {
  List<Employee> _employees;
  EmployeeListResponse employeeResponse;
  EmployeeManager employeeManager = EmployeeManager();
  String _searchQuery;
  Widget bodyContent;
  LoadingStatus loadingStatus = LoadingStatus.LOADING;
  NetworkUtils _networkUtils;

  @override
  void initState() {
    super.initState();
    _networkUtils = NetworkUtils(onNetworkChanged: getEmployee);
    _networkUtils.init();
  }

  void getEmployee() {
    setState(() {
      loadingStatus = LoadingStatus.LOADING;
    });

    employeeManager.getEmployees()
        .then((userResponse) =>
        setState(() {
          loadingStatus = LoadingStatus.SUCCESS;
          employeeResponse = userResponse;
          _employees = userResponse.employees;
        }))
        .catchError((error) {
      if (error is SocketException) {
        setState(() {
          loadingStatus = LoadingStatus.ERROR;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    bodyContent = _buildListEmployee();
    if (_employees != null && _employees.length == 0)
      bodyContent = _buildNoResult();

    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0.0,
        title: SearchBar(
          onChanged: _onSearch,
          onSubmit: _onSearch,
          onClear: _clearSearchQuery,
        ),
      ),
      body: ScreenStateView(
        status: loadingStatus,
        loadingContent: const PlatformProgressIndicator(),
        errorContent: const NetworkErrorView(),
        successContent: bodyContent,
      ),
    );
  }

  ListView _buildListEmployee() {
    return ListView.builder(
      itemCount: _employees == null ? 0 : _employees.length,
      itemBuilder: (context, index) {
        return ListItemEmployee(_employees[index]);
      },
    );
  }

  Widget _buildNoResult() {
    return BaseStateView(
      assetIcon: ImageAssets.searchNoResult,
      title: 'No results found for "$_searchQuery"',
    );
  }

  void _clearSearchQuery() {
    setState(() {
      if (employeeResponse != null)
        _employees = employeeResponse.employees;
    });
  }

  void _onSearch(String value) {
    setState(() {
      _searchQuery = value;
      _employees = employeeResponse.employees.where((employee) =>
          employee.getFullName().toLowerCase().contains(value.toLowerCase())).toList();
    });
  }

  @override
  void dispose() {
    _networkUtils.dispose();
    super.dispose();
  }
}
