import 'package:arqathon_flutter_quick_start/resources/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class IOSSearchBar extends StatelessWidget {
  final TextEditingController searchQuery;
  final ValueChanged<String> onChanged;
  final ValueChanged<String> onSubmit;
  final VoidCallback onClear;
  final VoidCallback onStop;
  final bool isSearching;
  final FocusNode onFocus;

  IOSSearchBar({
    @required this.searchQuery,
    @required this.onChanged,
    @required this.onSubmit,
    @required this.onClear,
    @required this.onStop,
    @required this.isSearching,
    @required this.onFocus,
  });

  @override
  Widget build(BuildContext context) {
    return _iOSSearchBar();
  }

  Widget _iOSSearchBar() {
    List<Widget> _iosBuilder = <Widget>[
      Expanded(
        child: Container(
          margin: EdgeInsets.only(right: (isSearching) ? 11.0 : 0.0),
          decoration: ShapeDecoration(
              color: Colors.grey[300].withOpacity(0.1),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              )),
          child: _buildSearchField(),
        ),
      )
    ];
    if (isSearching) {
      _iosBuilder.add(_buildCancelButton());
    }
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        child: Row(children: _iosBuilder));
  }

  TextField _buildSearchField() {
    return TextField(
      controller: searchQuery,
      focusNode: onFocus,
      onSubmitted: onSubmit,
      onChanged: onChanged,
      decoration: InputDecoration(
        prefixIcon: _buildSearchButton(),
        suffixIcon: _buildCloseButton(),
        hintText: Strings.searchHint,
        border: InputBorder.none,
        hintStyle: TextStyle(color: Colors.grey[700], fontSize: 16.0),
      ),
      style: const TextStyle(color: Colors.white, fontSize: 16.0),
    );
  }

  IconButton _buildCloseButton() {
    return (isSearching)
        ? IconButton(
            icon: Icon(
              FontAwesomeIcons.solidTimesCircle,
              color: Colors.grey[700],
              size: 16.0,
            ),
            onPressed: onClear,
          )
        : null;
  }

  IconButton _buildSearchButton() {
    return IconButton(
      icon: Icon(
        FontAwesomeIcons.search,
        color: Colors.grey[700],
        size: 16.0,
      ),
      onPressed: null,
    );
  }

  CupertinoButton _buildCancelButton() {
    return CupertinoButton(
      padding: EdgeInsets.zero,
      child: const Tooltip(
        message: 'Cancel',
        child: const Text(
          'Cancel',
          style: const TextStyle(color: Colors.white),
        ),
        excludeFromSemantics: true,
      ),
      onPressed: onStop,
    );
  }
}
