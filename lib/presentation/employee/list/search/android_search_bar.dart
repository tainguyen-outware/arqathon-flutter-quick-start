import 'package:arqathon_flutter_quick_start/resources/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AndroidSearchBar extends StatelessWidget {
  final TextEditingController searchQuery;
  final ValueChanged<String> onChanged;
  final ValueChanged<String> onSubmit;
  final VoidCallback onClear;
  final VoidCallback onStop;
  final bool isSearching;
  final FocusNode onFocus;

  AndroidSearchBar({
    @required this.searchQuery,
    @required this.onChanged,
    @required this.onSubmit,
    @required this.onClear,
    @required this.onStop,
    @required this.isSearching,
    @required this.onFocus,
  });

  @override
  Widget build(BuildContext context) {
    return _androidSearchBar();
  }

  Widget _buildSearchIcon() {
    return IconButton(
        icon: Icon(
          (isSearching) ? Icons.arrow_back : Icons.search,
          color: (isSearching) ? Colors.black : Colors.grey[700],
        ),
        onPressed: (isSearching) ? onStop : null);
  }

  Widget _buildSearchField() {
    return Expanded(
      child: TextField(
        focusNode: onFocus,
        onSubmitted: onSubmit,
        onChanged: onChanged,
        controller: searchQuery,
        decoration: InputDecoration(
          hintText: Strings.searchHint,
          border: InputBorder.none,
          hintStyle: const TextStyle(color: Colors.black38, fontSize: 17.0),
        ),
        style: const TextStyle(color: Colors.black87, fontSize: 17.0),
      ),
    );
  }

  Widget _buildCloseIcon() {
    return IconButton(
      icon: const Icon(
        Icons.clear,
        color: Colors.black87,
      ),
      onPressed: onClear,
    );
  }

  Widget _androidSearchBar() {
    List<Widget> _androidBuilder = List<Widget>();
    _androidBuilder.add(_buildSearchIcon());
    _androidBuilder.add(_buildSearchField());
    if (searchQuery.text.isNotEmpty) {
      _androidBuilder.add(_buildCloseIcon());
    }
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.symmetric(
          vertical: (isSearching) ? 0.0 : 4.0,
          horizontal: (isSearching) ? 0.0 : 8.0),
      decoration: ShapeDecoration(
          color: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular((isSearching) ? 0.0 : 4.0),
          )),
      child: Row(children: _androidBuilder),
    );
  }
}
