import 'dart:io';

import 'package:arqathon_flutter_quick_start/presentation/employee/list/search/android_search_bar.dart';
import 'package:arqathon_flutter_quick_start/presentation/employee/list/search/ios_search_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SearchBar extends StatefulWidget {
  final ValueChanged<String> onChanged;
  final ValueChanged<String> onSubmit;
  final VoidCallback onClear;

  SearchBar({
    this.onChanged,
    this.onSubmit,
    this.onClear,
  });

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  TextEditingController _searchQuery;
  bool _isSearching;
  FocusNode _onFocus = new FocusNode();

  @override
  void initState() {
    super.initState();
    _searchQuery = TextEditingController();
    _isSearching = _searchQuery.text.isNotEmpty;
    _onFocus.addListener(_onFocusChange);
  }

  @override
  Widget build(BuildContext context) {
    return Platform.isIOS ? IOSSearchBar(
      onFocus: _onFocus,
      searchQuery: _searchQuery,
      onChanged: widget.onChanged,
      onSubmit: widget.onSubmit,
      isSearching: _isSearching,
      onClear: _clearSearchQuery,
      onStop: _stopSearching,
    ) : AndroidSearchBar(
      searchQuery: _searchQuery,
      isSearching: _isSearching,
      onFocus: _onFocus,
      onStop: _stopSearching,
      onClear: _clearSearchQuery,
      onChanged: widget.onChanged,
      onSubmit: widget.onSubmit,
    );
  }

  void _stopSearching() {
    _isSearching = false;
    FocusScope.of(context).requestFocus(FocusNode());
    _clearSearchQuery();
  }

  void _onFocusChange() {
    setState(() {
      if (_onFocus.hasFocus) _isSearching = true;
    });
  }

  void _clearSearchQuery() {
    widget.onClear();
    setState(() {
      _searchQuery.clear();
    });
  }
}
