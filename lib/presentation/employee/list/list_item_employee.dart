import 'package:arqathon_flutter_quick_start/domain/models/employee.dart';
import 'package:arqathon_flutter_quick_start/presentation/employee/profile/employee_profile.dart';
import 'package:arqathon_flutter_quick_start/resources/assets.dart';
import 'package:flutter/material.dart';

class ListItemEmployee extends StatelessWidget {
  final Employee employee;

  ListItemEmployee(this.employee);

  void _navigateToEmployeeDetails(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) {
          return EmployeeProfile(
            employee: employee,
          );
        },
      ),
    );
  }

  Widget _buildProfileImage() {
    if (employee.avatar == null) {
      return null;
    }
    return ClipRRect(
        borderRadius: BorderRadius.circular(8.0),
        child: Align(
          alignment: Alignment.center,
          child: FadeInImage(
            placeholder: AssetImage(ImageAssets.loadingAvatar),
            image: NetworkImage(employee.avatar),
            width: 62.0,
            height: 62.0,
            fadeInDuration: const Duration(milliseconds: 300),
            fit: BoxFit.cover,
          ),
        ));
  }

  Widget _buildEmployeeInfo() {
    return Expanded(
      child: Container(
        padding: const EdgeInsets.only(left: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  employee.getFullName(),
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Text(
                  employee.location,
                  textAlign: TextAlign.justify,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
            const SizedBox(height: 4.0),
            Text(
              employee.role,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 16.0,
              ),
            ),
            const SizedBox(height: 4.0),
            Text(
              employee.email,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.left,
              style: const TextStyle(
                color: const Color(0xFF80ffffff),
                fontSize: 12.0,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: () => _navigateToEmployeeDetails(context),
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            children: <Widget>[
              _buildProfileImage(),
              _buildEmployeeInfo(),
            ],
          ),
        ),
      ),
    );
  }
}
