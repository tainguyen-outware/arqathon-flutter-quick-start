import 'dart:io';

import 'package:arqathon_flutter_quick_start/app_themes.dart';
import 'package:arqathon_flutter_quick_start/data/managers/employee_manager.dart';
import 'package:arqathon_flutter_quick_start/domain/models/employee.dart';
import 'package:arqathon_flutter_quick_start/domain/models/skill.dart';
import 'package:arqathon_flutter_quick_start/presentation/employee/profile/chip_tile.dart';
import 'package:arqathon_flutter_quick_start/presentation/employee/profile/heading.dart';
import 'package:arqathon_flutter_quick_start/presentation/employee/profile/social.dart';
import 'package:arqathon_flutter_quick_start/presentation/employee/profile/user_info.dart';
import 'package:arqathon_flutter_quick_start/resources/assets.dart';
import 'package:arqathon_flutter_quick_start/resources/dimensions.dart';
import 'package:arqathon_flutter_quick_start/resources/strings.dart';
import 'package:arqathon_flutter_quick_start/utils/widget_utils.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart' as DeepLinkLauncher;


class EmployeeProfile extends StatefulWidget {
  static const String routeName = '/profile';

  final Employee employee;

  EmployeeProfile({Key key, this.employee}) : super(key: key);

  @override
  _EmployeeProfileState createState() => _EmployeeProfileState();
}

class _EmployeeProfileState extends State<EmployeeProfile> {
  ScrollController _scrollController = ScrollController();
  EmployeeManager employeeManager = EmployeeManager();
  Employee employee;
  bool isLoading;
  bool _showTitle = false;

  @override
  void initState() {
    super.initState();
    this.employee = this.widget.employee;
    _scrollController.addListener(() {
      setState(() {
        bool _showTitle = _scrollController.hasClients && _scrollController.offset >
            Dimensions.scrollableAppBarHeight - kToolbarHeight * 1.5;

        /// Update state only when _showTitle has changed
        if (this._showTitle != _showTitle)
          this._showTitle = _showTitle;
      });
    });
    isLoading = true;
    employeeManager.getEmployeeDetails(employee.id).then((employee) {
      this.setState(() {
        this.employee = employee;
        isLoading = false;
      });
    }).catchError((error) {});
  }


  @override
  Widget build(BuildContext context) {
    List<Widget> mainContent = List();
    addIfNonNull(_buildUserProfile(), mainContent);
    addIfNonNull(_buildSkillSection(), mainContent);
    addAllIfNonNull(_buildCertificateSection(), mainContent);
    addIfNonNull(_buildCertificateList(), mainContent);

    return Scaffold(
      body: CustomScrollView(controller: _scrollController, slivers: <Widget>[
        SliverAppBar(
          expandedHeight: Dimensions.scrollableAppBarHeight,
          pinned: true,
          title: _showTitle ? Text(employee.getFullName()) : null,
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.create),
              tooltip: Strings.edit,
              onPressed: () {},
            )
          ],
          flexibleSpace: _buildExpendedTitle(),
        ),
        _buildSocialList(),
        SliverList(delegate: SliverChildListDelegate(mainContent)),
      ]),
    );
  }

  Widget _buildSocialList() {
    return SliverList(
      delegate: SliverChildListDelegate(<Widget>[
        Row(
          children: <Widget>[
            Social(
              icon: Icons.phone,
              text: Strings.call,
              onPressed: () => DeepLinkLauncher.launch("tel:${employee.phone}"),
            ),
            Social(
              icon: Icons.email,
              text: Strings.email,
              onPressed: () => DeepLinkLauncher.launch("mailto:${employee.email}"),
            ),
            Social(
              icon: FontAwesomeIcons.linkedin,
              text: Strings.linkedIn,
              onPressed: () => _launchLinkedIn(employee.linkedIn),
            ),
            Social(
              icon: FontAwesomeIcons.slack,
              text: Strings.slack,
              onPressed: () =>
                  _launchedSlack(employee.slack),
            ),
          ],
        ),
      ]),
    );
  }

  _launchLinkedIn(String link) async {
    if (await DeepLinkLauncher.canLaunch(link)) {
      DeepLinkLauncher.launch(link);
    }
    else {
      DeepLinkLauncher.launch(
          Platform.isAndroid ? Strings.linkedInGooglePlay : Strings.linkedInAppleStore);
    }
  }

  _launchedSlack(String link) async {
    if (await DeepLinkLauncher.canLaunch(link)) {
      DeepLinkLauncher.launch(link);
    }
    else {
      DeepLinkLauncher.launch(
          Platform.isAndroid ? Strings.slackGooglePlay : Strings.slackAppleStore);
    }
  }

  Widget _buildAppbarBackground() {
    return Container(
      height: Dimensions.scrollableAppBarHeight,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(Colors.grey[700].withOpacity(0.32), BlendMode.srcATop),
          image: NetworkImage(
            employee.avatar,
          ),
        ),
      ),
    );
  }

  Widget _buildExpendedTitle() {
    return _showTitle
        ? null
        : FlexibleSpaceBar(
      centerTitle: true,
      title: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Text(
            employee.getFullName(),
            style: TextStyle(
              fontSize: 20.0,
              color: Colors.white,
            ),
          ),
          Text(
            employee.role,
            style: TextStyle(
              fontSize: 12.0,
              color: Colors.white70,
              fontWeight: FontWeight.w300,
            ),
          ),
        ],
      ),
      background: _buildAppbarBackground(),
    );
  }

  Widget _buildUserProfile() {
    if (isLoading) {
      return null;
    }
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Divider(
            height: 0.0,
            color: Colors.grey[700],
          ),
          Heading(Strings.profile),
          UserInfo(
            icon: Icons.pin_drop,
            title: Strings.location,
            content: employee.location,
          ),
          UserInfo(
            icon: Icons.cake,
            title: Strings.birthDay,
            content: employee.dateOfBirth,
          ),
          UserInfo(
            icon: Icons.supervised_user_circle,
            title: Strings.team,
            content: employee.team,
          ),
          UserInfo(
            assetIcon: ImageAssets.howToReg,
            title: Strings.manager,
            content: employee.manager,
          ),
          UserInfo(
            assetIcon: ImageAssets.calendar,
            title: Strings.startDate,
            content: employee.startDate,
          ),
          Divider(
            height: 0.0,
            color: Colors.grey[700],
          )
        ],
      ),
    );
  }

  Widget _buildCertificateList() {
    if (isLoading) {
      return null;
    }
    List<String> certificates = <String>[
      "https://image.freepik.com/free-vector/certificate-template-design_1262-2535.jpg",
      "https://s3.amazonaws.com/images.seroundtable.com/google-power-searching-certificate-1343216578.jpg",
      "http://gtsak.info/wp-content/uploads/2018/04/microsoft-certificate-templates-microsoft-template-certificate-templates-ideas.png",
      "https://marketplace.canva.com/MAB9dWbMBLE/1/0/thumbnail_large/canva-colourful-puzzle-pieces-achievement-certificate-MAB9dWbMBLE.jpg",
      "https://assets.online.berklee.edu/certificate-images/music-marketing-professional.jpg",
    ];

    return SizedBox.fromSize(
      size: const Size.fromHeight(88.0),
      child: ListView.builder(
        padding: const EdgeInsets.only(right: 16.0, left: 16.0, bottom: 16.0),
        scrollDirection: Axis.horizontal,
        itemCount: certificates.length,
        itemBuilder: (BuildContext context, int index) {
          bool isLast = certificates.length == (index + 1);
          return _buildCertificate(isLast, certificates[index]);
        },
      ),
    );
  }

  Widget _buildCertificate(bool isLast, String certificate) {
    if (isLoading) {
      return null;
    }
    return Container(
      margin: !isLast ? const EdgeInsets.only(right: 8.0) : null,
      child: ClipRRect(
          borderRadius: BorderRadius.circular(8.0),
          child: Align(
            alignment: Alignment.center,
            child: FadeInImage(
              placeholder: AssetImage(ImageAssets.loadingAvatar),
              image: NetworkImage(certificate),
              fadeInDuration: const Duration(milliseconds: 300),
              fit: BoxFit.cover,
            ),
          )),
    );
  }


  List<Widget> _buildCertificateSection() {
    if (isLoading) {
      return null;
    }
    return <Widget>[
      Container(
        margin: const EdgeInsets.only(left: 16.0),
        child: Heading(Strings.certificate),
      )
    ];
  }

  Widget _buildSkillSection() {
    if (isLoading) {
      return null;
    }
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Heading(Strings.skills),
          ChipTheme(
            data: kDarkTheme.data.chipTheme,
            child: ChipTile(children: _buildListChips()),
          ),
          const SizedBox(height: 16.0,),
          Divider(
            height: 0.0,
            color: Colors.grey[700],
          )
        ],
      ),
    );
  }

  List<Widget> _buildListChips() {
    return employee.skills.map<Widget>((Skill skill) {
      return Chip(
        backgroundColor: Colors.grey[900],
        key: ValueKey<String>(skill.name),
        label: Text(
          skill.name,
          style: const TextStyle(fontWeight: FontWeight.w400, fontSize: 14.0, color: Colors.white),
        ),
      );
    }).toList();
  }
}
