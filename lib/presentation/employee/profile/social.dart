import 'package:flutter/material.dart';

class Social extends StatelessWidget {
  final IconData icon;
  final String text;
  final VoidCallback onPressed;

  Social({@required this.icon, @required this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        onTap: onPressed,
        child: Column(
          children: <Widget>[
            const SizedBox(height: 12.0,),
            Icon(
              icon,
              size: 24.0,
              color: Colors.white,
            ),
            const SizedBox(height: 8.0,),
            Text(text,
                style: TextStyle(
                  fontSize: 12.0,
                  color: Colors.white70,
                )),
            const SizedBox(height: 12.0,)
          ],
        ),
      ),
    );
  }
}
