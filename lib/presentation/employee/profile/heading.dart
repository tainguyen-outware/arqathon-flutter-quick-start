import 'package:flutter/material.dart';

class Heading extends StatelessWidget {
  final String text;

  Heading(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 0.0),
      child: Text(
        text,
        style: TextStyle(fontSize: 17.0, color: Colors.grey[700], fontWeight: FontWeight.w600),
      ),
    );
  }
}
