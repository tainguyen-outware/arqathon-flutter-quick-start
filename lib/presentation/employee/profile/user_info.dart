import 'package:flutter/material.dart';

class UserInfo extends StatelessWidget {
  final String content;
  final String title;
  final IconData icon;
  final String assetIcon;

  UserInfo({@required this.content, @required this.title, this.icon, this.assetIcon});

  Widget _buildIcon() {
    if (icon != null) {
      return Icon(
        icon,
        size: 24.0,
        color: Colors.grey[700],
      );
    } else if (assetIcon != null) {
      return ImageIcon(
        AssetImage(assetIcon),
        size: 24.0,
        color: Colors.grey[700],
      );
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 16.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _buildIcon(),
                  const SizedBox(width: 32.0,),
                  Text(
                    content ?? "",
                    style: TextStyle(fontSize: 15.0, color: Colors.grey[300]),
                  ),
                ],
              ),
              Container(
                margin: const EdgeInsets.only(left: 56.0),
                child: Text(
                  title ?? "",
                  style: TextStyle(
                    fontSize: 12.0,
                    color: Colors.white30,
                  ),
                ),
              ),
            ]));
  }
}
