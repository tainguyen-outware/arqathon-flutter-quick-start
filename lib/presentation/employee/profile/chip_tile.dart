import 'package:flutter/material.dart';

class ChipTile extends StatelessWidget {
  const ChipTile({
    Key key,
    this.children,
  }) : super(key: key);

  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.all(0.0),
      title: Wrap(
        crossAxisAlignment: WrapCrossAlignment.start,
        alignment: WrapAlignment.start,
        runAlignment: WrapAlignment.start,
        spacing: 8.0,
        // gap between adjacent chips
        runSpacing: 8.0,
        children: children.map((Widget widget) => widget).toList(),
      ),
    );
  }
}
