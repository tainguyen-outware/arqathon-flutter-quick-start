import 'package:arqathon_flutter_quick_start/presentation/employee/list/employee_list.dart';
import 'package:arqathon_flutter_quick_start/resources/strings.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  static const String routeName = '/login';

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(16.0),
        margin: const EdgeInsets.only(top: kToolbarHeight),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            const Text(
              Strings.loginWelcome,
              style: const TextStyle(
                fontSize: 34.0,
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 54.0, bottom: 16.0),
              child: FlatButton(
                child: Text(
                  Strings.btnLogin,
                  style: const TextStyle(
                      fontSize: 17.0, fontWeight: FontWeight.w600),
                ),
                onPressed: () => _onLogin(context),
                color: Colors.blueAccent[200],
                padding: const EdgeInsets.all(16.0),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
              ),
            ),
            FlatButton(
              child: Text(
                Strings.btnForgotPassword,
                style: const TextStyle(fontSize: 17.0),
              ),
              onPressed: () => {},
              padding: const EdgeInsets.all(16.0),
              textColor: Colors.blueAccent[200],
            ),
          ],
        ),
      ),
    );
  }

  void _onLogin(BuildContext context) {
    Navigator.of(context).pushReplacementNamed(EmployeeList.routeName);
  }
}
