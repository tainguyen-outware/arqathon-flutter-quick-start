import 'package:arqathon_flutter_quick_start/presentation/common/base_state_view.dart';
import 'package:arqathon_flutter_quick_start/resources/strings.dart';
import 'package:flutter/material.dart';

class NetworkErrorView extends BaseStateView {
  const NetworkErrorView({
    String title,
    String description,
  }) : super(
    icon: Icons.signal_wifi_off,
    title: title ?? Strings.networkErrorTitle,
    description: description ?? Strings.networkErrorDescription,
  );
}
