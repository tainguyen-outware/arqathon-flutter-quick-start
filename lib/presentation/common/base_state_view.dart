import 'package:arqathon_flutter_quick_start/resources/strings.dart';
import 'package:arqathon_flutter_quick_start/utils/widget_utils.dart';
import 'package:flutter/material.dart';

class BaseStateView extends StatelessWidget {
  const BaseStateView({
    Key key,
    this.title,
    this.description,
    this.icon,
    this.assetIcon,
    this.iconSize,
    this.iconColor,
    this.actionButtonKey,
    this.onActionButtonTapped,
  }) : super(key: key);

  final Key actionButtonKey;
  final String title;
  final IconData icon;
  final double iconSize;
  final String assetIcon;
  final Color iconColor;
  final String description;
  final VoidCallback onActionButtonTapped;

  Widget _buildActionButton(BuildContext context) {
    if (onActionButtonTapped == null) {
      return null;
    }
    return Padding(
      padding: const EdgeInsets.only(top: 12.0),
      child: FlatButton(
        key: actionButtonKey,
        onPressed: onActionButtonTapped,
        child: Text(
          Strings.reload,
//          style: TextStyle(color: Theme.of(context).primaryColor),
        ),
      ),
    );
  }

  Widget _buildIcon(BuildContext context) {
    if (assetIcon != null) {
      return ImageIcon(
        AssetImage(assetIcon),
        size: 40.0,
        color: Colors.grey[700],
      );
    }
    else {
      return Icon(
        icon ?? Icons.info,
        color: iconColor ?? Colors.grey[700],
        size: iconSize ?? 56.0,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> content = <Widget>[_buildIcon(context)];
    addAllIfNonNull(_buildTitle(context), content);
    addAllIfNonNull(_buildDescription(), content);
    addIfNonNull(_buildActionButton(context), content);
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: content,
          ),
        ),
      ),
    );
  }

  List<Widget> _buildTitle(BuildContext context) {
    return (title == null)
        ? List()
        : <Widget>[
      const SizedBox(height: 16.0),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Text(
          title,
          style: const TextStyle(fontSize: 14.0),
        ),
      )
    ];
  }

  List<Widget> _buildDescription() {
    return (description == null)
        ? List()
        : <Widget>[
      const SizedBox(height: 8.0),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32.0),
        child: Text(
          description,
          textAlign: TextAlign.center,
          style: const TextStyle(fontSize: 12.0),
        ),
      )
    ];
  }
}
