import 'package:arqathon_flutter_quick_start/presentation/common/base_state_view.dart';
import 'package:arqathon_flutter_quick_start/resources/strings.dart';
import 'package:flutter/material.dart';

class ErrorView extends BaseStateView {
  const ErrorView({
    String title,
    String description,
    VoidCallback onRetry,
  }) : super(
          icon: Icons.error,
          title: title ?? Strings.errorTitle,
          description: description ?? Strings.errorDescription,
          onActionButtonTapped: onRetry,
        );
}
