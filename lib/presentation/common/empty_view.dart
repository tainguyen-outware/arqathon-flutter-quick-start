import 'package:arqathon_flutter_quick_start/presentation/common/base_state_view.dart';
import 'package:arqathon_flutter_quick_start/resources/strings.dart';
import 'package:flutter/material.dart';

class EmptyView extends BaseStateView {
  const EmptyView({
    String title,
    String description,
    @required VoidCallback onRetry,
  }) : super(
          icon: Icons.content_paste,
          title: title ?? Strings.emptyTitle,
          description: description ?? Strings.emptyDescription,
          onActionButtonTapped: onRetry,
        );
}
