import 'package:arqathon_flutter_quick_start/presentation/employee/list/employee_list.dart';
import 'package:arqathon_flutter_quick_start/presentation/login/login.dart';
import 'package:flutter/material.dart';

class Screen {
  const Screen({
    this.title,
    this.subtitle,
    @required this.routeName,
    @required this.buildRoute,
  })  : assert(routeName != null),
        assert(buildRoute != null);

  final String title;
  final String subtitle;
  final String routeName;
  final WidgetBuilder buildRoute;

  @override
  String toString() {
    return '$runtimeType($title $routeName)';
  }
}

List<Screen> _buildScreens() {
  final List<Screen> screens = <Screen>[
    Screen(
      routeName: EmployeeList.routeName,
      buildRoute: (BuildContext context) => EmployeeList(),
    ),
    Screen(
      routeName: Login.routeName,
      buildRoute: (BuildContext context) => Login(),
    ),
  ];

  return screens;
}

final List<Screen> kAllScreens = _buildScreens();
