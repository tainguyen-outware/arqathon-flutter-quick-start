class Strings {
  Strings._();

  static const String appName = "Arqathon Quick Start";

  static const String linkedInGooglePlay = "https://play.google.com/store/apps/details?id=com.linkedin.android";
  static const String linkedInAppleStore = "https://itunes.apple.com/au/app/id288429040";
  static const String slackGooglePlay = "https://play.google.com/store/apps/details?id=com.Slack";
  static const String slackAppleStore = "https://itunes.apple.com/au/app/id803453959";

  ///Platforms
  static const String platform = "Platform";
  static const String androidPlatform = "Android";
  static const String iosPlatform = "iOS";

  ///Theme
  static const String darkTheme = "Dark Theme";
  static const String lightTheme = "Light Theme";

  ///Font Size
  static const String textSize = "Text Size";
  static const String defaultSize = "Default";
  static const String smallSize = "Small";
  static const String normalSize = "Normal";
  static const String largeSize = "Large";
  static const String hugeSize = "Huge";

  /// Page's titles
  static const String setting = "Setting";

  /// State View
  static const String reload = "Reload";
  static const String errorTitle = "Oops!";
  static const String errorDescription =
      "There was an error while\nloading data.";
  static const String emptyTitle = "Empty!";
  static const String emptyDescription = "There is no data at the moment.";
  static const String networkErrorTitle = "Oops, no internet connection!";
  static const String networkErrorDescription = "Please wield your internet power.";

  /// Login
  static const String loginWelcome = "Welcome,\nArqadian.";
  static const String btnLogin = "Login";
  static const String btnForgotPassword = "Forgot your password?";

  /// User Profile
  static const String call = "Call";
  static const String email = "Email";
  static const String linkedIn = "Linkedin";
  static const String slack = "Slack";
  static const String profile = "Profile";
  static const String birthDay = "Birthday";
  static const String location = "Location";
  static const String team = "Team";
  static const String manager = "Manager";
  static const String startDate = "Start Date";
  static const String skills = "Skills";
  static const String certificate = "Certificates";

  /// Actions
  static const String edit = "Edit";
  static const String searchHint = "Search Arquadians & powers";

}
