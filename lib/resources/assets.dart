class ImageAssets {
  static const String loadingAvatar = 'assets/images/loading_avatar.gif';
  static const String howToReg = 'assets/images/how_to_reg.png';
  static const String calendar = 'assets/images/calendar.png';
  static const String searchNoResult = 'assets/images/search_no_result.png';
}

class AudioAssets {}

class VideoAssets {}
