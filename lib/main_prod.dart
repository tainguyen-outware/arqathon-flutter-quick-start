import 'package:arqathon_flutter_quick_start/app.dart';
import 'package:arqathon_flutter_quick_start/environment.dart';
import 'package:flutter/material.dart';

void main() {
  Environment.production();
  assert(environment != null);
  runApp(const ArqHubApp());
}
