import 'package:intl/intl.dart';

class DateTimeUtils {
  static final DateFormat dateFormat = new DateFormat('dd/MM/yyyy');
  static final DateFormat timeFormat = new DateFormat('HH:mm');
}
