import 'package:flutter/widgets.dart';

void addIfNonNull(Widget widget, List<Widget> children) {
  if (widget != null) {
    children.add(widget);
  }
}

void addAllIfNonNull(List<Widget> widget, List<Widget> children) {
  if (widget != null && widget.length > 0) {
    children.addAll(widget);
  }
}
