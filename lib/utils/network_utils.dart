import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

class NetworkUtils {
  VoidCallback onNetworkChanged;
  NetworkUtils({@required this.onNetworkChanged});

  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  void init() {
    onNetworkChanged();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(
        (ConnectivityResult result) => processConnectivityResult(result));
  }

  void dispose() {
    _connectivitySubscription.cancel();
  }

  void processConnectivityResult(ConnectivityResult connectivityResult) {
    switch (connectivityResult) {
      case ConnectivityResult.wifi:
        onNetworkChanged();
        break;
      case ConnectivityResult.mobile:
        onNetworkChanged();
        break;
      case ConnectivityResult.none:
      //TODO show no internet banner when internet goes off after data has been loaded.
        break;
    }
  }
}
