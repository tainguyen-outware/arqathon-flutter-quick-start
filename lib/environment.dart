enum BuildFlavor { production, development }

Environment get environment => _environment;
Environment _environment;

class Environment {
  final String apiBaseUrl;
  final BuildFlavor buildFlavor;

  Environment._init({this.buildFlavor, this.apiBaseUrl});

  static void development() =>
      _environment = Environment._init(
      buildFlavor: BuildFlavor.development,
      apiBaseUrl: 'http://agilestub-staging.omdev.io/stub/arq_hub/');

  static void production() =>
      _environment = Environment._init(
      buildFlavor: BuildFlavor.production,
      apiBaseUrl: 'http://13.236.94.31:8080/');
}
