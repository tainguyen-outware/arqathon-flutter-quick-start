import 'package:arqathon_flutter_quick_start/app_themes.dart';
import 'package:arqathon_flutter_quick_start/presentation/login/login.dart';
import 'package:arqathon_flutter_quick_start/resources/strings.dart';
import 'package:arqathon_flutter_quick_start/routes.dart';
import 'package:flutter/material.dart';

class ArqHubApp extends StatefulWidget {
  const ArqHubApp({
    Key key,
    this.testMode = false,
  }) : super(key: key);
  final bool testMode;

  @override
  _ArqHubAppState createState() => _ArqHubAppState();
}

class _ArqHubAppState extends State<ArqHubApp> {
  Map<String, WidgetBuilder> _buildRoutes() {
    return new Map<String, WidgetBuilder>.fromIterable(
      kAllScreens,
      key: (dynamic route) => '${route.routeName}',
      value: (dynamic route) => route.buildRoute,
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: kDarkTheme.data,
      title: Strings.appName,
      color: Colors.grey,
      routes: _buildRoutes(),
      home: Login(),
    );
  }
}
