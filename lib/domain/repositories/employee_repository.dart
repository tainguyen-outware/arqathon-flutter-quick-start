import 'dart:async';

import 'package:arqathon_flutter_quick_start/data/api/models/employee_list_response.dart';
import 'package:arqathon_flutter_quick_start/domain/models/employee.dart';

abstract class EmployeeRepository {
  Future<EmployeeListResponse> getEmployees();

  Future<Employee> getEmployeeDetails(String employeeId);
}
