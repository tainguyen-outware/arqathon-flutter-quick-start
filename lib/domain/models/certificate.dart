import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'certificate.g.dart';

@JsonSerializable()
class Certificate extends Object with _$CertificateSerializerMixin {
  final String id;

  final String logo;

  final String name;

  Certificate({
    @required this.id,
    @required this.logo,
    @required this.name,
  });

  factory Certificate.fromJson(Map<String, dynamic> json) =>
      _$CertificateFromJson(json);
}
