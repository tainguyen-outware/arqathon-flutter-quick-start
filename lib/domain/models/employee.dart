import 'package:arqathon_flutter_quick_start/domain/models/certificate.dart';
import 'package:arqathon_flutter_quick_start/domain/models/skill.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'employee.g.dart';

@JsonSerializable()
class Employee extends Object with _$EmployeeSerializerMixin {
  @JsonKey(name: 'id')
  final String id;

  @JsonKey(name: 'first_name')
  final String firstName;

  @JsonKey(name: 'last_name')
  final String lastName;

  @JsonKey(name: 'role')
  final String role;

  @JsonKey(name: 'email')
  final String email;

  @JsonKey(name: 'location')
  final String location;

  @JsonKey(name: 'avatar')
  final String avatar;

  @JsonKey(name: 'date_of_birth')
  final String dateOfBirth;

  @JsonKey(name: 'team')
  final String team;

  @JsonKey(name: 'manager')
  final String manager;

  @JsonKey(name: 'start_date')
  final String startDate;

  @JsonKey(name: 'phone')
  final String phone;

  @JsonKey(name: 'linkedin')
  final String linkedIn;

  @JsonKey(name: 'slack')
  final String slack;

  @JsonKey(name: 'skills')
  final List<Skill> skills;

  @JsonKey(name: 'certificates')
  final List<Certificate> certificates;

  Employee({
    this.dateOfBirth,
    this.team,
    this.manager,
    this.startDate,
    this.phone,
    this.linkedIn,
    this.slack,
    @required this.id,
    @required this.firstName,
    @required this.lastName,
    @required this.role,
    @required this.email,
    @required this.location,
    @required this.avatar,
    this.skills,
    this.certificates,
  });

  factory Employee.fromJson(Map<String, dynamic> json) =>
      _$EmployeeFromJson(json);

  String getFullName() => "$firstName $lastName";
}
