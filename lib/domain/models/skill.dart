import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'skill.g.dart';

@JsonSerializable()
class Skill extends Object with _$SkillSerializerMixin {
  final String id;

  final String name;

  Skill({
    @required this.id,
    @required this.name,
  });

  factory Skill.fromJson(Map<String, dynamic> json) => _$SkillFromJson(json);
}
