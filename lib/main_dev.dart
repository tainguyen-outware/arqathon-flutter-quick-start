import 'package:arqathon_flutter_quick_start/app.dart';
import 'package:arqathon_flutter_quick_start/environment.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void main() {
  Environment.development();
  assert(environment != null);
  debugInstrumentationEnabled = true;
  MaterialPageRoute.debugEnableFadingRoutes = true; // ignore: deprecated_member_use
  runApp(const ArqHubApp());
}