# arqathon_flutter_quick_start

## Intro

The goal of this project is to create a project that displays list of employees and their details. Main features include:

- Mock Login screen
- List of employees
- Loading screen 
- No network connection screen
- Employee details screen
- Deep links

## Environments
- Development: run the **main_dev.dart** file
- Production: run the **main_prod.dart** file

## Setup
- Setup Flutter. Please visit [Flutter official website](https://flutter.io/get-started/install/) for installation guide

- Clone the project
    `git clone https://tainguyen-outware@bitbucket.org/tainguyen-outware/arqathon-flutter-quick-start.git`
- Install flutter packages
    `flutter packages get`
- Generate JSON serialization code for model classes
    `flutter packages pub run build_runner build`

## Extended Reading
- [Useful Resources](https://drive.google.com/open?id=1NoVT8Q4UfpQqLootsPBi-o98y0hkMtG-Ih1finPKk4U)
- [Flutter official website](https://flutter.io/)


